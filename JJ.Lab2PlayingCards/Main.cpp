
// Lab Exercise 2 - Playing Cards
// Jason Jeffers

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

// rank enum
enum Rank { TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE };
// suit enum
enum Suit { SPADES = 1, CLUBS, DIAMONDS, HEARTS };

// card struct
struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card);


Card HighCard(Card card1, Card card2);






int main()
{
	Card card1;
	Card card2;

	card1.Rank = QUEEN;
	card1.Suit = DIAMONDS;

	card2.Rank = FIVE;
	card2.Suit = HEARTS;

	PrintCard(card1);

	PrintCard(card2);

	PrintCard(HighCard(card1, card2));
	

	(void)_getch();
	return 0;
}

void PrintCard(Card card)
{
	cout << "The " << card.Rank << " of " << card.Suit << "\n";
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank)
	{
		return card1;
	}
	else if (card1.Rank < card2.Rank)
	{
		return card2;
	}
	else
	{
		return card1, card2;
	}
	
}

